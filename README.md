# BeQualified

Das Projekt ist als public auf GitLab hochgeladen. 
Das Programm wurde mit Eclipse auf einem MacOS Rechner mit einem Google-Chrome Browser getestet. 

Mit der Suchmaschine "Google" läuft das Programm einwandfrei. Für andere Suchmaschinen (wie z.B. "Bing" oder "Yahoo") müssen einige Anpassungen gemacht werden. Die kann ich gerne beim Vorstellungsgespräch erklären. 