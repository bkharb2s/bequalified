package drive;

import java.util.List;
import java.util.Scanner;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class DriveLuncher {
	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "../chromedriver");
		Scanner scanner = new Scanner(System.in);
		System.out.println("please give the search machine you want ");
		String url = scanner.nextLine();
		System.out.println("please write the word that you looking for :");
		String wordToSearch = scanner.nextLine();
		System.out.println("please give me the number of the first sites you want to show");
		int numberofSites =Integer.parseInt(scanner.nextLine());
		WebDriver driver = new ChromeDriver();

		driver.get("http://www.google.com");
		Thread.sleep(2000); 
		WebElement searchBox = driver.findElement(By.name("q"));
		searchBox.sendKeys(url);
		searchBox.sendKeys(Keys.ENTER);
		WebElement searchmachine =driver.findElement(By.tagName("cite"));
		searchmachine.click();
		WebElement newSearchBox = driver.findElement(By.name("q"));

		newSearchBox.sendKeys(wordToSearch);
		newSearchBox.sendKeys(Keys.RETURN);
		Thread.sleep(2000);
		String result = driver.findElement(By.id("resultStats")).getText();
		System.out.println(result);
		System.out.println("the first "+numberofSites +" sites are :");
		List<WebElement> numberOfSearch = driver.findElements(By.tagName("cite"));
		for(int i=0;i<numberofSites;i++) {
			System.out.println(i+":"+numberOfSearch.get(i).getText());
		}
	}
}
